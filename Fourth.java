/*A program to demonstrate serialization for
various different dependencies*/


import java.io.*;
class first implements Serializable{
String place;
int age;
first(String place,int age)
{
	this.place=place;
	this.age=age;
}
}


public class second implements Serializable{
	int id;
	String name;
	second(int id, String name)
	{
		this.id=id;
		this.name=name;
		
	}
//System.out.println(this.id);
//System.out.println(this.name);
}


//Java code for serialization and deserialization  
//of a Java object 
import java.io.*; 
class third extends first //implements Serializable 
{  
 public int a; 
 public String b; 
 second D; 
 // Default constructor 
 public third(String place, int age, int a, String b,int id,String name) 
 { 
    super(place,age);
     D = new second( id, name);
     this.a = a; 
     this.b = b; 
 } 
} 

class fourth 
{ 
 public static void main(String[] args) 
 {    
     third object = new third("hyderabad",51,1, "thing",10,"bhagi"); 
     String filename = "D:\\bha.txt"; 
       
     // Serialization  
     try
     {    
         //Saving of object in a file 
         FileOutputStream file = new FileOutputStream(filename); 
         ObjectOutputStream out = new ObjectOutputStream(file); 
           
         // Method for serialization of object 
         out.writeObject(object); 
           
         out.close(); 
         file.close(); 
           
         System.out.println("Object has been serialized"); 

     } 
       
     catch(IOException ex) 
     { 
         System.out.println("IOException is caught"+ex); 
     } 
   }
   }  


   